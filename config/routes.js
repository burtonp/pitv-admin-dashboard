/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    controller: 'HomepageController',
    action: 'index',
    locals: {
      appName: "Crud Demo - Sail.js",
      currentView: "Welcome",
      viewIndex: 0
    }
  },

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

  // PAGES

  '/users': {
    view: 'users',
    locals: {
      appName: "Crud Demo - Sail.js",
      currentView: "Users",
      viewIndex: 1
    }
  },

  '/contacts': {
    controller: 'ContactController',
    action: 'init',
    locals: {
      appName: "Crud Demo - Sail.js",
      currentView: "Contacts",
      viewIndex: 2
    }
  },

  '/images': {
    controller: 'ImageController',
    action: 'init',
    locals: {
      appName: "Crud Demo - Sail.js",
      currentView: "Images",
      viewIndex: 3
    }
  },

  '/myProfile': {
    view: 'user/myProfile',
    locals: {
      appName: "Crud Demo - Sail.js",
      currentView: "My Profile",
      viewIndex: 7
    }
  },

  '/logout': {
    controller: 'UserController',
      action: "logout",
      locals: {
      viewIndex: 6
    }
  },

  // HTTP REQUESTS

  'get /login': {
    view: 'user/login',
    locals: {
      appName: "Crud Demo - Sail.js",
      currentView: "Login",
      locals: {
        appName: "Crud Demo - Sail.js",
        currentView: "Login",
        viewIndex: 5
      }
    }
  },

  'get /signup': {
    view: 'user/signup',
    locals: {
      appName: "Crud Demo - Sail.js",
      currentView: "Sign Up",
      viewIndex: 4
    }
  },

  'post /login': {
    controller: 'UserController',
    action: 'login'
  },

  'post /signup': {
    controller: 'UserController',
    action: 'signup'
  },

  //'post /createUser': 'UserController.createUser',

  // get /user
  // post /user
  // get /user/:id
  // put /user/:id
  // delete /user/:id

};
