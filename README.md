# SailsJS-Demo
a [Sails](http://sailsjs.org) application

## Overview
- Currently uses Sails' built in localDiskDb for data storage. This can eventually be configured to use mongo or a data store of your choice. see: ```/config/connections.js```
- Since we are using a local db, when you first run the app there will be no data and you must be logged in to use the app. So goto: ```http://localhost:1337/signup``` to create an account.
- Sails Documentation: ```http://sailsjs.org/documentation/concepts/```
 
## Goal
- Simple CRUD demo (more info coming soon)

## Issue Tracking
[TODO]

## Quick Start
1. Clone the repo and checkout develop
2. `npm install`
3. `sails lift`
4. Navigate to [http://localhost:1337](http://localhost:1337)

## API
1. Currently you can view data by going to the respective url
2. For example: to view all users: http://localhost:1337/user