app.controller('BaseCtrl', [
  '$scope',
  '$mdDialog',
  '$mdToast',
  '$mdMedia', function($scope, $mdDialog, $mdToast, $mdMedia) {

    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('sm');

    // Once we retreive all of our data, expose it to angular
    // USERS
    io.socket.get('/user', function(data) {
      $scope.users = data;
      $scope.$apply();
    });
    // CONTACTS
    io.socket.get('/contact', function(data) {
      $scope.contacts = data;
      $scope.$apply();
    });
    // IMAGES
    io.socket.get('/image', function(data) {
      $scope.images = data;
      $scope.$apply();
    });

    // Listen to actions on the user model
    // TODO: do the same for other models
    io.socket.on('user', function(event) {
      switch (event.verb) {
        case 'created' :
          $scope.users.push(event.data);
          $scope.$apply();
          break;
        case 'updated' :
          // TODO: Abstract into its own function
          var users = $scope.users,
            userIndex;

          angular.forEach(users, function(user, key) {
            if(user.id === event.id) {
              userIndex = key;
              return;
            }
          }, userIndex);

          // TODO:Check for difference between previous and new
          users[userIndex].firstName = event.data.firstName;
          users[userIndex].lastName = event.data.lastName;
          users[userIndex].email = event.data.email;
          users[userIndex].updatedAt = event.data.updatedAt;
          $scope.$apply();
          break;
        case 'destroyed' :
          $scope.users.pop(event.previous);
          $scope.$apply();
          break;
        default :
          break;
      }
    });

    io.socket.on('contact', function(event) {
      switch (event.verb) {
        case 'created' :
          $scope.contacts.push(event.data);
          $scope.$apply();
          break;
        case 'updated' :
          // TODO: Abstract into its own function
          var contacts = $scope.contacts,
            users = $scope.users,
            contactIndex,
            userIndex;

          angular.forEach(contacts, function(contact, key) {
            if(contact.id === event.id) {
              contactIndex = key;
              return;
            }
          }, contactIndex);

          angular.forEach(users, function(user, key) {
            if(user.id === event.data.user) {
              userIndex = key;
              return;
            }
          }, userIndex);

          // TODO:Check for difference between previous and new
          contacts[contactIndex].type = event.data.type;
          contacts[contactIndex].value = event.data.value;
          contacts[contactIndex].user = users[userIndex];
          contacts[contactIndex].updatedAt = event.data.updatedAt;
          $scope.$apply();
          break;
        case 'destroyed' :
          $scope.contacts.pop(event.previous);
          $scope.$apply();
          break;
        default :
          break;
      }
    });

    // TODO: MOVE TO APPROPRIATE CONTROLLER
    $scope.launchContact = function(contact, event) {

      $scope.currentContact = angular.copy(contact);

      $mdDialog.show({
        controller: 'ContactModalController',
        templateUrl: 'templates/contact-modal.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: event,
        clickOutsideToClose:true,
        fullscreen: $mdMedia('sm') && $scope.customFullscreen,
        locals: {
          currentContact: $scope.currentContact,
          users: $scope.users
        }
      }).then(function(message) {
        $scope.status = message;
        $mdToast.show(
          $mdToast.simple()
            .textContent($scope.status)
            .position('bottom right')
            .hideDelay(3000)
        );
      }, function() {
        $scope.status = 'Dialog closed, operation canceled';
        $mdToast.show(
          $mdToast.simple()
            .textContent($scope.status)
            .position('bottom right')
            .hideDelay(3000)
        );
      });

      $scope.$watch(function() {
        return $mdMedia('sm');
      }, function(sm) {
        $scope.customFullscreen = (sm === true);
      });
    };

    // TODO: MOVE TO APPROPRIATE CONTROLLER
    $scope.launchUser = function(user, event) {

      $scope.currentUser = angular.copy(user);

      //debugger;
      $mdDialog.show({
        controller: 'UserModalController',
        templateUrl: 'templates/user-modal.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: event,
        clickOutsideToClose:true,
        fullscreen: $mdMedia('sm') && $scope.customFullscreen,
        locals: {
          currentUser: $scope.currentUser
        }
      }).then(function(message) {
        $scope.status = message;
        $mdToast.show(
          $mdToast.simple()
            .textContent($scope.status)
            .position('bottom right')
            .hideDelay(3000)
        );
      }, function() {
        $scope.status = 'Dialog closed, operation canceled';
        $mdToast.show(
          $mdToast.simple()
            .textContent($scope.status)
            .position('bottom right')
            .hideDelay(3000)
        );
      });

      $scope.$watch(function() {
        return $mdMedia('sm');
      }, function(sm) {
        $scope.customFullscreen = (sm === true);
      });

    };

  }
]);
