app.controller('CreateContactsCtrl', [
  '$scope',
  '$http', function($scope, $http) {

    // Set default
    $scope.contactType = "mobile";

    $scope.createContactSubmit = function(event) {
      var data = {
        user: $scope.userSelector,
        type: $scope.contactType,
        value: $scope.contactValue
      };

      $http.post('/contact', data).then(
        function success(response) {
          // TODO: Clear form
          //debugger;
        },
        function error(response) {
          // TODO: Handle error, Clear form
          //debugger;
        }
      );
    };
  }
]);
