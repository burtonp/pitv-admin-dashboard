app.controller('UploadImageCtrl', ['$scope', '$http', function($scope, $http) {
  $scope.uploadImage = function(event) {

    //var fileInput = event.target;
    var fileInput = document.getElementById('fileSelector'),
        fileReader = new FileReader(),
        file = fileInput.files[0];

    /*fileReader.onloadend = function(e){
      var data = e.target.result;
      debugger;
      //send you binary data via $http or $resource or do anything else with it
    };

    fileReader.readAsArrayBuffer(file);
     */

    var data = {
      extension: file.name.split('.').pop(),
      name: file.name.substr(0, file.name.indexOf('.')),
      path: file.name,
      caption: $scope.imgCaption,
      user: $scope.userSelector
    };


    $http.post('/image', data).then(
      function success(response) {
        // TODO: Clear form
        debugger;
      },
      function error(response) {
        // TODO: Handle error, Clear form
        debugger;
      }
    );
  }
}]);
