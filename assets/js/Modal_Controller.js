/**
 * User Modal
 */
app.controller('UserModalController', [
  '$scope',
  '$http',
  '$mdDialog',
  '$mdToast',
  'currentUser', function($scope, $http, $mdDialog, $mdToast, currentUser) {

    $scope.currentUser = currentUser;

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.updateUser = function(user) {
      var path = '/user/' + user.id,
        data = {
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email
        };

      $http.put(path, data).then(
        function success(response) {
          var message = "User ID: " + response.data.id + " Updated";
          $mdDialog.hide(message);
        },
        function error(response) {
          var message = "User ID: " + response.data.id + " ERROR: There was an issue updating the record.";
          $mdToast.show(
            $mdToast.simple()
              .textContent(message)
              .position('bottom right')
              .hideDelay(3000)
          );
        }
      );
    };

    $scope.deleteUser = function(id) {
      var path = '/user/' + id;

      $http.delete(path).then(
        function success(response) {
          var message = "User: " + response.data.id + " Deleted";
          $mdDialog.hide(message);
        },
        function error(response) {
          debugger;
          var message = "User ID: " + response.data.id + " ERROR: There was an issue deleting the record.";
          $mdToast.show(
            $mdToast.simple()
              .textContent(message)
              .position('bottom right')
              .hideDelay(3000)
          );
        }
      );
    };

  }
]);

/**
 * Contact Modal
 */
app.controller('ContactModalController', [
  '$scope',
  '$http',
  '$mdDialog',
  '$mdToast',
  'currentContact',
  'users', function($scope, $http, $mdDialog, $mdToast, currentContact, users) {

    $scope.currentContact = currentContact;
    $scope.users = users;

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.updateContact = function(contact) {
      var path = '/contact/' + contact.id,
        data = {
          type: contact.type,
          value: contact.value,
          user: contact.user.id
        };

      $http.put(path, data).then(
        function success(response) {
          var message = "Contact ID: " + response.data.id + " Updated";
          $mdDialog.hide(message);
        },
        function error(response) {
          var message = "Contact ID: " + response.data.id + " ERROR: There was an issue updating the record.";
          $mdToast.show(
            $mdToast.simple()
              .textContent(message)
              .position('bottom right')
              .hideDelay(3000)
          );
        }
      );
    };

    $scope.deleteContact = function(id) {
      var path = '/contact/' + id;

      $http.delete(path).then(
        function success(response) {
          var message = "Contact: " + response.data.id + " Deleted";
          $mdDialog.hide(message);
        },
        function error(response) {
          var message = "Contact ID: " + response.data.id + " ERROR: There was an issue deleting the record.";
          $mdToast.show(
            $mdToast.simple()
              .textContent(message)
              .position('bottom right')
              .hideDelay(3000)
          );
        }
      );
    };

  }
]);
