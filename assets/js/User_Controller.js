app.controller('CreateUserCtrl', [
  '$scope',
  '$http', function($scope, $http) {
    $scope.createUserSubmit = function(event) {
      var data = {
        firstName: $scope.firstName,
        lastName: $scope.lastName,
        email: $scope.email,
        password: $scope.password
      };

      $http.post('/user', data).then(
        function success(response) {
          // TODO: Clear form
          //debugger;
        },
        function error(response) {
          // TODO: Handle Error, Clear form
          //debugger;
        }
      );
    };
  }
]);
