/**
 * HomepageController
 *
 * @description :: Server-side logic for managing homepages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  _config: {
    actions: false
  },
  index: function (req, res) {
    if(!req.session.me) req.session.me = {};
    return res.view('homepage');
  }
};

