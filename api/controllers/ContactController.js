/**
 * ContactController
 *
 * @description :: Server-side logic for managing contacts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  init: function (req, res) {
    if(!req.session.me) req.session.me = {};
    return res.view('contacts');
  }
};

