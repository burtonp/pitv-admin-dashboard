/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * `UserController.login()`
   */
  login: function (req, res) {

    // See `api/responses/login.js`
    return res.login({
      email: req.param('email'),
      password: req.param('password'),
      successRedirect: '/myProfile',
      invalidRedirect: '/login'
    });
  },

  /**
   * `UserController.logout()`
   */
  logout: function (req, res) {

    // "Forget" the user from the session.
    // Subsequent requests from this user agent will NOT have `req.session.me`.
    req.session.me = null;

    // If this is not an HTML-wanting browser, e.g. AJAX/sockets/cURL/etc.,
    // send a simple response letting the user agent know they were logged out
    // successfully.
    if (req.wantsJSON) {
      return res.ok('Logged out successfully!');
    }

    // Otherwise if this is an HTML-wanting browser, do a redirect.
    return res.redirect('/');
  },

  /**
   * `UserController.signup()`
   */
  signup: function (req, res) {

    // Attempt to signup a user using the provided parameters
    User.signup({
      firstName: req.param('firstName'),
      lastName: req.param('lastName'),
      email: req.param('email'),
      password: req.param('password')
    }, function (err, user) {
      // res.negotiate() will determine if this is a validation error
      // or some kind of unexpected server error, then call `res.badRequest()`
      // or `res.serverError()` accordingly.
      if (err) return res.negotiate(err);

      // Go ahead and log this user in as well.
      // We do this by "remembering" the user in the session.
      // Subsequent requests from this user agent will have `req.session.me` set.
      req.session.me = user;

      // If this is not an HTML-wanting browser, e.g. AJAX/sockets/cURL/etc.,
      // send a 200 response letting the user agent know the signup was successful.
      if (req.wantsJSON) {
        return res.ok('Signup successful!');
      }

      // Otherwise if this is an HTML-wanting browser, redirect to /welcome.
      return res.redirect('/myProfile');

    });
  },

  // Blueprint Actions, implemented under the hood for quick prototyping of data objects,
  // can be overridden...

  //  find: function(req, res) {},
  //  findOne: function(req, res) {},
  //  create: function(req, res) {},
  //  update: function(req, res) {},
  //  destroy: function(req, res) {}

  // GET /user/:id
  /*findOne : function(req, res) {
    User.findOne({
      id: req.param(id)
    }).exec(function(err, user) {
      if(err) return res.negotiate(err);

      // Subscribe to changes to this particular user record
      // (will send socket events)
      //User.subscribe(req, user.id);

      return res.view('/profile', {
        user: user,
        pageTitle: "Welcome, " + user.firstName
      });

    });
  }*/

  /*findOne: function(request, response) {
    Person.find(request.params.id).exec(function (error, persons) {
      var person = persons[0];
      person.fullName = person.firstName + ' ' + person.lastName;
      response.json(person);
    });
  }*/
};

