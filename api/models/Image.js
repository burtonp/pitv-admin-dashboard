/**
* Image.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    name: {
      type: 'string',
      size: 128,
      required: true
    },
    extension: {
      type: 'string',
      size: 128,
      required: true
    },
    path: {
      type: 'string',
      size: 128,
      required: true
    },
    caption: {
      type: 'string',
      size: 128
    },
    user: {
      //type: 'json'
      model: 'User'
    }
  }
};

