/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    firstName: {
      type: 'string',
      size: 128,
      required: true
    },
    lastName: {
      type: 'string',
      size: 128,
      required: true
    },
    email: {
      type: 'email',
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
    contacts: {
      collection: 'Contact',
      via: 'user'
    },
    images: {
      collection: 'Image',
      via: 'user'
    }
  },

  /**
   * User.signup()
   * Create a new user using the provided inputs,
   * but encrypt the password first.
   *
   * @param  {Object}   inputs
   *  • firstName {String}
   *  • lastName  {String}
   *  • email     {String}
   *  • password  {String}
   * @param  {Function} cb
   */
  signup: function (inputs, cb) {
    // Create a user
    User.update({
      avatarURL: "http://placedog.com/200/300"
    }).exec(cb);
  },

  /**
   * User.attemptLogin()
   * Check validness of a login using the provided inputs.
   * But encrypt the password first.
   *
   * @param  {Object} inputs
   *  • email    {String}
   *  • password {String}
   * @param  {Function} cb
   */
  attemptLogin: function (inputs, cb) {
    // Create a user
    User.findOne({
      email: inputs.email,
      password: inputs.password // TODO: Encrypt the password first
    }).exec(cb);
  }

};

